;AT89S52 Test Devresi Programi

SIFRE EQU 0X16	; TC numaras�n�n son iki hanesi

;Reset vekt�r�
ORG 0000H
JMP START

ORG 0100H
;Ana porogram baslangi�
START:
;Baslangi� ayarlari
CLR A						;Ak�m�lat�r� temizle
MOV P3,A					;Port3 �ikislarini sifirla
MOV A,#0XFF					;Ak�m�lat�re 0xff sabitini y�kle
MOV P0,A					;Port0 bitlerini giris i�in hazirla (Port0 y�ksek empedans)
;Ana dong�
LOOP:
MOV A,P0  					;Port0'i oku	
MOV P1,A  					;Port1'e yaz
CPL A	
MOV P2,A 
CPL A					    ;Ak�mlat�re� Port2'ye yaz
CJNE A,#SIFRE,SONDUR	    ; Ak�m�lat�r i�erigi ile sifre1'i karsilastir.
SETB P3.0					;Esit ise P3.0 bitini set et
JMP LOOP					;Degilse P3.0 bitini temizle 					;
SONDUR:
CLR P3.0					
JMP LOOP					;D�ng�y� tekrarla
END
